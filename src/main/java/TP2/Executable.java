package TP2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.nio.AttributeType;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.csv.CSVFormat;
import org.jgrapht.nio.csv.CSVImporter;
import org.jgrapht.nio.dot.DOTExporter;

public class Executable {

	public static Graph<String, DefaultEdge> load(String nom_fichier) {
		/*
		Charge le fichier csv "nom_fichier" et renvoie le graphe associé
		*/
		Graph<String, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class);
		CSVImporter<String, DefaultEdge> importer = new CSVImporter<>(CSVFormat.EDGE_LIST);
		importer.setVertexFactory(id -> id);
		importer.importGraph(graph, new File(nom_fichier));
		return graph;
	}

	public static void export(Graph<String, DefaultEdge> graph, String nom_fichier) throws IOException{
		/* 
		Exporte le graphe au format dot et l'écrit dans le fichier nom_fichier
		*/
		DOTExporter<String, DefaultEdge> exporter = new DOTExporter<String, DefaultEdge>();
		exporter.setVertexAttributeProvider((x) -> Map.of("label", new DefaultAttribute<>(x, AttributeType.STRING)));
		exporter.exportGraph(graph, new FileWriter(nom_fichier));

	}

	public static void main(String[] args) throws IOException {
		Graph<String, DefaultEdge> graph = load("vienna_subway.csv");
        
        // Pour instancier un graphe vide :
        // Graph<String, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class); // 
		export(graph, "graph.dot");

	}

}
