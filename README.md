# Code java pour l'utilisation de la bibiothèque jgrapht


## TP2

Exécutez le fichier Executable.java du répertoire sr/main/java/TP2 depuis vscode. 

Cela génère un fichie "graph.dot", vous pouvez y jeter un oeil, mais on ne s'y intéressera pas particulièrement.

Transformez ce fichier en un .pdf en exécutant : <code>  dot -T pdf graph.dot -o graph.pdf </code>

Vous pouvez alors visualiser le pdf : <code> atril graph.pdf </code>